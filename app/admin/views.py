from sqlalchemy import Column, Integer, String
from werkzeug.security import generate_password_hash, check_password_hash
from app.cars.models import Vin
from fastadmin import SqlAlchemyModelAdmin, register
from sqlalchemy import select
from app.database import async_session_maker, Base


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    admin_name = Column(String(50), nullable=False, unique=True)
    password = Column(String(), nullable=False)

    @staticmethod
    def set_password(password):
        password = generate_password_hash(password)
        return password

    def check_password(self, password):
        return check_password_hash(self.password, password)


@register(User, sqlalchemy_sessionmaker=async_session_maker)
class UserAdmin(SqlAlchemyModelAdmin):
    exclude = ("password",)
    list_display = ("id", "admin_name")
    list_display_links = ("id", "admin_name")
    list_filter = ("id", "admin_name",)
    search_fields = ("admin_name",)

    async def authenticate(self, username, password):
        sessionmaker = self.get_sessionmaker()
        async with sessionmaker() as session:
            query = select(User).filter_by(admin_name=username)
            result = await session.scalars(query)
            user = result.first()
            if not user:
                return None
            if not user.check_password(password):
                return None
            return user.id


@register(Vin, sqlalchemy_sessionmaker=async_session_maker)
class CarAdmin(SqlAlchemyModelAdmin):
    list_display = ("vin", "make", "model", "type", "color", "dimensions", "weight")

    list_display_links = ("vin", "make", "model", "type", "color", "dimensions", "weight")
    list_filter = ("vin", "make", "model", "type", "color", "dimensions", "weight")
    search_fields = ("vin", "make", "model", "type", "color", "dimensions", "weight")

    async def authenticate(self, username, password):
        sessionmaker = self.get_sessionmaker()
        async with sessionmaker() as session:
            query = select(User).filter_by(admin_name=username)
            result = await session.scalars(query)
            user = result.first()
            if not user:
                return None
            if not user.check_password(password):
                return None
            return user.id
