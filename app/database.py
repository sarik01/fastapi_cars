from typing import AsyncGenerator, Sequence

from sqlalchemy import insert, update, delete, select, Update, Insert, Delete, RowMapping
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine, async_sessionmaker
from sqlalchemy.orm import declarative_base

# from .config import DB_HOST, DB_NAME, DB_PASS, DB_PORT, DB_USER

DATABASE_URL = f"postgresql+asyncpg://{'postgres'}:{'123456'}@{'localhost'}:{'5432'}/{'fastapi_cars'}"

Base = declarative_base()

engine = create_async_engine(DATABASE_URL)
async_session_maker = async_sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


class BaseModel(Base):
    __abstract__ = True

    def __init__(self, session: AsyncSession):
        """

        :param session:
        """
        self.session = session

    def base_format(self):
        """

        :return:
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    async def save(self, query: Update | Insert | Delete):
        """

        :param query:
        :return:
        """
        await self.session.execute(query)
        await self.session.commit()

    async def add(self, data) -> None:
        """

        :param data:
        :return:
        """
        query = insert(self.__class__).values(**data.dict())
        await self.save(query)

    async def update(self, item_id: int, data) -> None:
        """

        :param item_id:
        :param data:
        :return:
        """
        query = update(self.__class__).where(self.__class__.id == item_id).values(**data.dict())
        await self.save(query)

    async def delete(self, item_id: int) -> None:
        """

        :param item_id:
        :return:
        """
        query = delete(self.__class__).where(self.__class__.id == item_id)
        await self.save(query)

    async def get(self, item_id: int) -> dict:
        """

        :param item_id:
        :return:
        """
        execution = await self.session.execute(select(self.__class__).where(self.__class__.id == item_id))
        result: BaseModel = execution.scalar()
        return result.base_format()

    async def get_all(self) -> Sequence[RowMapping]:
        """

        :return:
        """
        execution = await self.session.execute(select(self.__class__))
        result = execution.scalars()
        return [x.base_format() for x in result]
