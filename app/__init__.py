from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from app.cars.routes import router
from fastadmin import fastapi_app as admin_app


def create_app() -> FastAPI:
    from app.admin.views import UserAdmin
    app = FastAPI(title='Cars VIN API')
    app.include_router(router)

    app.mount("/admin", admin_app)

    origins = [
        "http://localhost:3000",
    ]

    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["GET", "POST", "OPTIONS", "DELETE", "PATCH", "PUT"],
        allow_headers=["Content-Type", "Set-Cookie",
                       "Access-Control-Allow-Headers",
                       "Access-Control-Allow-Origin",
                       "Authorization"],
    )

    return app
