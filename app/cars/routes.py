# from typing import Dict
#
# import httpx
from typing import List

from fastapi import APIRouter, Depends
# from fastapi import Response, status
# from sqlalchemy import select, insert
# from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession

# from app.admin.views import User
from app.cars.models import Vin

from app.database import get_async_session

from app.cars.schemas import CarSchema, GetCarSchema

router = APIRouter(
    prefix='/private/vin/decode',
    tags=["VIN"]
)

STATUS_SUCCESS = {'status': 'success'}

# @router.get('/{vin}/', status_code=200)
# async def get_post(vin: str,
#                    response: Response,
#                    session: AsyncSession = Depends(get_async_session)) -> dict:
#     """
#
#     :param vin:
#     :param response:
#     :param session:
#     :return:
#     """
#     query = select(Vin).where(Vin.vin == vin)
#     execute = await Operations.get_one(session, query, response)
#     return execute
#
#
# @router.get('/', status_code=200)
# async def get_post_req(session: AsyncSession = Depends(get_async_session)):
#     """
#
#
#     :param session:
#     :return:
#     """
#     try:
#         async with httpx.AsyncClient() as client:
#             req = await client.get('http://localhost:8000/private/vin/decode/JN8DR07XX1W514175/')
#             dct = req.json()
#             dct['vin'] = 'JN8DR07XX1W5141754'
#             print(dct)
#             await Operations.insert(session, Vin, **dct)
#
#             return req.json()
#     except NoResultFound:
#         return {'status': 'not found!'}, 404


# @router.post('/admin/')
# async def create_admin(session: AsyncSession = Depends(get_async_session)):
#     query = await session.execute(select(User).where(User.admin_name == 'admin'))
#     admin = query.first()
#
#     if admin is None:
#         password = User.set_password(password='123456')
#
#         await Operations.execute_plus_commit(session, stmt)
#         print('Admin Created!')
#         return 'admin_create!'


@router.post('/', status_code=201)
async def add_post(new_post: CarSchema,
                   session: AsyncSession = Depends(get_async_session)):
    """

    :param new_post:
    :param session:
    :return:
    """
    vin = Vin(session)

    await vin.add(new_post)

    return STATUS_SUCCESS


@router.put('/')
async def upd_post(item_id: int, new_post: CarSchema,
                   session: AsyncSession = Depends(get_async_session)):
    """

    :param item_id:
    :param new_post:
    :param session:
    :return:
    """
    vin = Vin(session)

    await vin.update(item_id, new_post)

    return STATUS_SUCCESS


@router.delete('/', status_code=204)
async def del_post(item_id: int,
                   session: AsyncSession = Depends(get_async_session)):
    """

    :param item_id:
    :param session:
    :return:
    """
    vin = Vin(session)

    await vin.delete(item_id)

    return STATUS_SUCCESS


@router.get('/', response_model=GetCarSchema)
async def get_post(item_id: int,
                   session: AsyncSession = Depends(get_async_session)):
    """

    :param item_id:
    :param session:
    :return:
    """
    vin = Vin(session)

    return await vin.get(item_id)


@router.get('/all', response_model=List[GetCarSchema])
async def get_posts(session: AsyncSession = Depends(get_async_session)):
    """

    :param session:
    :return:
    """
    vin = Vin(session)

    return await vin.get_all()
