from sqlalchemy import Column, Integer, String

from app.database import Base, BaseModel


class Vin(BaseModel):
    __tablename__ = 'vin'

    id = Column(Integer, primary_key=True)
    vin = Column(String(256), unique=True, nullable=False)
    year = Column(Integer, nullable=False)
    make = Column(String(256), nullable=False)
    model = Column(String(256), nullable=False)
    type = Column(String(256), nullable=False)
    color = Column(String(256), nullable=False)
    dimensions = Column(String(256), nullable=False)
    weight = Column(String(256), nullable=False)

    def format(self):
        return {
            'make': self.make,
            'model': self.model,
            'type': self.type,
            'color': self.color,
            'dimensions': self.dimensions,
            'weight': self.weight,
            'year': self.year
        }
