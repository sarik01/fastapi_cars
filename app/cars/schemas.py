from pydantic import BaseModel


class CarSchema(BaseModel):
    vin: str
    year: int
    make: str
    model: str
    type: str
    color: str
    dimensions: str
    weight: str


class GetCarSchema(BaseModel):
    id: int
    vin: str
    year: int
    make: str
    model: str
    type: str
    color: str
    dimensions: str
    weight: str
