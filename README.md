<h1>Fastapi-Cars</h1>
<p>To run the application you need to have docker on your computer.</p>
<p>Once you are sure that you have it, open a terminal and run
<mark>docker-compose up</mark> command.</p>
<p>The application itself will install
all the dependencies for its operation and also install the Postgres database and create demo data.</p>
<p>Next, to test the application, open the terminal again and turn off the running
application with the command <mark>ctrl + c </mark>, make sure that the application has been stopped altogether.</p>
<p>Then run the command <mark>docker-compose run —rm fastapi sh -c 'pytest tests/' </mark>, if the test passes successfully,
turn on the application again Run the <mark>docker-compose up </mark> command, then open your browser and go to the link</p> <li><a href="http://localhost:8000/docs">http://localhost:8000/docs</a></li>

<p>After you open the shortcut, you will see the application documentation.</p>

![Swagger](imgs/swagger.png)
<p>GET request is the route that we need to request VIN.</p>
<p>Then, in order to execute the request, click on the request itself after the try it out desk and enter one of the following VIN:</p>

![try_it](imgs/try_it.png)
<li>SCBBR9ZA1AC063223</li>
<li>JN8DR07XX1W514175</li>
<li>1P3EW65F4VV300946</li>
<p>Then click on the execute button and scroll down the page and there you will see the response from the server in Json format.</p>

![execute](imgs/execute.png)
![response](imgs/response.png)

<p>The application also has an admin panel for performing CRUD operations.</p>
<p>To access it, go to the link <a href="http://localhost:8000/admin">http://localhost:8000/admin</a>.</p>
<p>You will be greeted by an authorization page.</p>

![autorization](imgs/admin_login.png)
<p>To log in,<mark> enter admin in the username cell and 123456 in the password cell </mark>.</p>
<p>After authorization, you will see a dashboard and on the left side of your screen there will be User and Vin buttons.</p>


![dashboard](imgs/adnin_dashboard.png)
<p>Next, click on Vin and you will be provided with information about each Vin that is stored in our database.</p>
<p>There you can add a new vin, change or delete the old one.</p>

![vin](imgs/vin.png)

