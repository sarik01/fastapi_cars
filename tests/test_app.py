from httpx import AsyncClient


async def test_add_first_vin(ac: AsyncClient):
    response = await ac.post("/private/vin/decode/", json={
        "vin": "SCBBR9ZA1AC063223",
        "year": 2010,
        "make": "BENTLEY",
        "model": "Continental",
        "type": "PASSENGER CAR",
        "color": "white",
        "dimensions": "Class 2E: 6,001 - 7,000 lb",
        "weight": "2,722 - 3,175 kg",
    })
    assert response.status_code == 200


async def test_get_first_vin(ac: AsyncClient):
    response = await ac.get("/private/vin/decode/SCBBR9ZA1AC063223/")
    assert response.status_code == 200


async def test_add_second_vin(ac: AsyncClient):
    response = await ac.post("/private/vin/decode/", json={
        "vin": "JN8DR07XX1W514175",
        "year": 2001,
        "make": "NISSAN",
        "model": "Pathfinder",
        "type": "MULTIPURPOSE PASSENGER VEHICLE (MPV)",
        "color": "red",
        "dimensions": "Class 1D: 5,001 - 6,000 lb",
        "weight": "2,268 - 2,722 kg",
    })
    assert response.status_code == 200


async def test_get_second_vin(ac: AsyncClient):
    response = await ac.get("/private/vin/decode/JN8DR07XX1W514175/")
    assert response.status_code == 200


async def test_add_third_vin(ac: AsyncClient):
    response = await ac.post("/private/vin/decode/", json={
        "vin": "1P3EW65F4VV300946",
        "year": 1997,
        "make": "PLYMOUTH",
        "model": "Prowler",
        "type": "	PASSENGER CAR",
        "color": "black",
        "dimensions": "Class 1D: 5,001 - 6,000 lb",
        "weight": "2,268 - 2,722 kg",
    })
    assert response.status_code == 200


async def test_get_third_vin(ac: AsyncClient):
    response = await ac.get("/private/vin/decode/1P3EW65F4VV300946/")
    assert response.status_code == 200


async def test_create_admin(ac: AsyncClient):
    response = await ac.post("/private/vin/decode/admin/")
    assert response.status_code == 200